#ifndef PLUGIN_H
#define PLUGIN_H

#pragma once

#include <string>
#include <vector>

namespace hewa::plugin
{
	class Plugin
	{
		private:
			std::string getName();
			std::string getVersion();
			std::vector<std::string> getAuthors();
			std::string getDescription();

			std::string _name;
			std::string _version;
			std::vector<std::string> _authors;
			std::string _desc;
			
		public:
			void name(std::string name);
			void version(std::string version);
			void authors(std::vector<std::string> authors);
			void description(std::string desc);
	};
}

#endif //PLUGIN_H